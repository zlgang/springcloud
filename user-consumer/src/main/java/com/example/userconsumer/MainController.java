package com.example.userconsumer;

import com.example.userconsumer.feignconsumer.FeignConsumer_0;
import com.example.userconsumer.feignconsumer.FeignConsumer_1;
import com.example.userconsumer.feignconsumer.FeignConsumer_2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
public class MainController {

    @Value("${server.port}")
    private int port;
    @Resource
    private FeignConsumer_0 feignConsumer0;

//    @Resource
//    private FeignConsumer_1 feignConsumer_1;

    @Resource
    private FeignConsumer_2 feignConsumer_2;

    @GetMapping("/alive")
    public String alive() {
        return feignConsumer0.alive();
    }

//    @GetMapping("/alive1")
//    public String alive1() {
//        return feignConsumer_1.alive();
//    }

    @GetMapping("/alive2")
    public String alive2() {
        return "consumer:port" + port + "--->" + feignConsumer_2.alive();
    }

    @GetMapping("/map")
    public Map getMap() {
        return feignConsumer_2.getMapParam(100, "memeda");
    }

    @GetMapping("/mapRest")
    public Map getMapRest() {
        return feignConsumer_2.getMapRest(100, "memeda");
    }

    @GetMapping("/getMapMapParam")
    public Map getMapMapParam() {
        Map map = new HashMap();
        map.put("name", "zlg");
        map.put("age", 18);
        map.put("id", 1);
        return feignConsumer_2.getMapRequestBody(map);
    }

    @GetMapping("/postMap")
    public Map postMap() {
        Map map = new HashMap();
        map.put("name", "zlg");
        map.put("age", 18);
        map.put("id", 1);
        return feignConsumer_2.postMap(map);
    }


}
