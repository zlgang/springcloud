package com.example.userconsumer.feignconsumer;

import com.example.userapi.UserApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * v2：使用user-api 引入jar方式调用
 * 此种方式在两个java项目之间调用非常方便
 * 其他异构系统调用不适用，如有异构系统调用，可再编写接口文档
 */
@FeignClient(name = "user-provider")
public interface FeignConsumer_2 extends UserApi {


    //----------------自定义api调用---------
    //普通url get方式调用，OpenFeign会自动识别@RequestParam参数，拼接到url上
    @GetMapping("/getMapParam")
    Map getMapParam(@RequestParam Integer id, @RequestParam String name);

    //restful接口调用，自动识别@PathVariable注解补充到url的参数
    @GetMapping("/getMapRest/{id}-{name}")
    Map getMapRest(@PathVariable Integer id, @PathVariable String name);

    //通过实体传多个参数
    @GetMapping("/getMapMapParam")
    Map getMapRequestBody(@RequestParam Map map);

    //post 提交数据
    @PostMapping("/postMap")
    Map postMap(@RequestBody Map map);
}
