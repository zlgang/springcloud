package com.example.userconsumer.feignconsumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * v0：脱离eureka，用feign模拟直接http调用
 *
 */
@FeignClient(name = "xxx", url = "http://localhost:81")
public interface FeignConsumer_0 {
    @GetMapping("/alive")
    public String alive();
}
