package com.example.userconsumer.feignconsumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * V1：声明资源api方式调用
 * 这种方式与RestTemplate无异，提供的参数、资源路径都是一样的
 * 实际就是封装了RestTemplate
 */
//指定服务
//@FeignClient(name = "user-provider")
public interface FeignConsumer_1 {
    @GetMapping("/alive")
    String alive();
}
