package com.example.userprovider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderController {

    @GetMapping("/alive")
    public String alive() {
        return "ok";
    }


}
