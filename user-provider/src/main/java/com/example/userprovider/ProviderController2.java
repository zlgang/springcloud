package com.example.userprovider;

import com.example.userapi.UserApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class ProviderController2 implements UserApi {

    @Value("${server.port}")
    private int port;

    @GetMapping("/alive")
    @Override
    public String alive() {
        return "provider:port" + port +  "ok";
    }

//-----------------------自定义服务-------------------

    @GetMapping("/getMapParam")
    public Map getMapParam(@RequestParam Integer id, @RequestParam String name) {
        return Collections.singletonMap(id, name);
    }


    //restful 接口
    @GetMapping("/getMapRest/{id}-{name}")
    public Map getMapRest(@PathVariable Integer id, @PathVariable String name) {
        return Collections.singletonMap(id, name);
    }

    //多个参数时，使用实体接收
    @GetMapping("/getMapMapParam")
    public Map getMapRequestBody(@RequestParam Map map) {
        return map;
    }

    //接收post 提交数据
    @PostMapping("/postMap")
    public Map postMap(@RequestBody Map map) {
        return map;
    }
}
