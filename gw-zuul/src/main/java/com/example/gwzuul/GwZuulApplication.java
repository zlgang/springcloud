package com.example.gwzuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy //启用zuul proxy
public class GwZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(GwZuulApplication.class, args);
    }

}
